import pickle

def pickle_load(pickle_file):
    with open(pickle_file, 'rb') as handle:
        return pickle.load(handle)    

def pickle_save(to_save,pickle_file='filename.pickle'):
    with open(pickle_file, 'wb') as handle:
        pickle.dump(to_save, handle, protocol=pickle.HIGHEST_PROTOCOL)    